function is_array(obj) {
	if (obj.constructor.toString().indexOf("Array") == -1)	return false;
	else													return true;
}

function is_object(obj) {
	if (typeof obj === "object")	return true;
	else							return false;
}

function is_numeric(strString)	{  //  check for valid numeric strings
	var strValidChars = "0123456789.-";
	var strChar;
	var blnResult = true;

	if (strString.length == 0) return false;

	//  test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++)	{
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)	{
			blnResult = false;
		}
	}

	return blnResult;
}

function sort_array(array, column)	{
	var size = array.length;
	var keep;

	for (var a = 0; a < (size - 1); a++) {
		for (var b = a + 1; b < size; b++) {
			if (array[a][column] == array[b][column]) {
				if (array[a][column] < array[b][column]) {
					keep = array[a];
					array[a] = array[b];
					array[b] = keep;
				}
			}
			else if (array[a][column] > array[b][column]) {
				keep = array[a];
				array[a] = array[b];
				array[b] = keep;
			}
		}
	}

	return array;
}