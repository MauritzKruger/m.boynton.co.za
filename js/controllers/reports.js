/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:13 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

	var periods		=  [
							{value:"last3",descr:"Last 3 Hours"},
							{value:"last6",descr:"Last 6 Hours"},
							{value:"last12",descr:"Last 12 Hours"},
							{value:"last24",descr:"Last 24 Hours"},
							{value:"currWeek",descr:"Current Week"},
							{value:"prevWeek",descr:"Previous Week"},
							{value:"currMonth",descr:"Current Month"},
							{value:"prevMonth",descr:"Previous Month"}
						];

    var Reports = can.Control({
        // Default options
        defaults : {
            view				: "views/reports.ejs",
			nodes			: ""
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options));
        },
        show: function(){
			this.options.menusMode.load();
            //console.log(this.options.credentials);
            this.element.html(can.view("views/reports.ejs", {data:[{nodes:this.options.nodes,periods:periods}]}));
            this.element.slideDown(200);
			$("div#final_step").hide();
            $("#menu ul li").removeClass("active");
            $("#reports").addClass("active");

			$("select.multiselect").multiSelect({showHeader:false});
			$(".ui-multiselect").attr("style", "width:226px;");
        },
        hide: function(){
            this.element.slideUp(200);
        },
        "{document} #reports click": function(){
            //if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	{
					that.options.menusMode.clearTimer();
					$.when(Models.Reports.findAll().then(function (findAllResponse) {
						that.options.nodes = findAllResponse;
						that.show();
					}));
				}
			}));
            //}
        },
        "{document} .get-parameters click": function(){
			var selected_nodes = "";

			var data = $("#report-form").serialize().split("&");

			for (var i = 0; i < data.length; i++)	{
				var entry = data[i].split("=");

				if (entry[0] == "node")	selected_nodes += ""+entry[1]+"__";
			}

			if (selected_nodes.length > 0)	{
				selected_nodes = selected_nodes.substring(0, (selected_nodes.length-2));

				$("div#final_step").show();

				$.when(Models.Reports.findOne({"nodes":selected_nodes}).then(function (findOneResponse) {
					var html = "<select id='param' name='param' class='multiselect' multiple='multiple' size='5'>";

					$.each(findOneResponse["parameters"],function(i, v)	{
						html += "<option value='"+v["value"]+"'>"+v["descr"]+"</option>";
					});

					html += "</select>";

					$("div#param_div").html(html);

					$("select#param").multiSelect({showHeader:false});
					$(".ui-multiselect").attr("style", "width:226px;");
				}));
			} else
				$("div#final_step").hide();
        },
        "{document} .graph click": function(){
			var selected_nodes = "";
			var selected_params = "";
			var selected_period = "";

			var data = $("#report-form").serialize().split("&");

			for (var i = 0; i < data.length; i++)	{
				var entry = data[i].split("=");

				if (entry[0] == "node")				selected_nodes += ""+entry[1]+"__";
				else if (entry[0] == "param")	selected_params += ""+entry[1]+"__";
				else if (entry[0] == "period")	selected_period += ""+entry[1];
			}

			if (selected_nodes.length > 0)	selected_nodes = selected_nodes.substring(0, (selected_nodes.length-2));
			if (selected_params.length > 0)	selected_params = selected_params.substring(0, (selected_params.length-2));

			if (selected_nodes.length > 0 && selected_params.length > 0)	{
				$.when(Models.Reports.create({"nodes":selected_nodes,"params": selected_params,"period": selected_period}).then(function (response) {
					$("div#result_div").html("");

					Theme.init ();

					selected_nodes = selected_nodes.split(/__/);
					selected_params = selected_params.split(/__/);

					$.each(selected_params,function(i,v){
						var innerHTML = "<center>";

							innerHTML += "<div class='graph-heading'>"+response[v]["descr"]+"</div>";
							innerHTML += "<br/>";
							innerHTML += "<div id='"+v+"'>Loading...</div>";

						innerHTML += "</center>";
						innerHTML += "<br/>";

						$("div#result_div").append(innerHTML);
					});

					$.each(selected_params,function(i,v){
						var data = [];

						$.each(selected_nodes,function(j,w){
							var dataset = [];

							if (is_object(response[v][w]["data"]))	{
								var resultSet = response[v][w]["data"];

								for( var i = 0; i < resultSet.length; i++ ) {
									var year = parseFloat(resultSet[i]["year"]);
									var month = parseFloat(resultSet[i]["month"]);
									var day = parseFloat(resultSet[i]["day"]);
									var hours = parseFloat(resultSet[i]["hours"]);
									var minutes = parseFloat(resultSet[i]["minutes"]);
									var seconds = parseFloat(resultSet[i]["seconds"]);
									var value = parseFloat(resultSet[i]["value"]).toFixed(3);

									dataset.push([new Date(year, month, day, hours, minutes, seconds), value]);
								}

								data.push({
													data: dataset,
													label: response[v][w]["label"],
													points: { show: false },
													lines: { lineWidth: 2, fill: false }
												});
							}
						});

						$("div#"+v).addClass("chart-holder");

						Charts.line ("#"+v, data);
						$("#"+v).UseTooltip();
					});
				}));
			}
        }
    });

    namespace.Reports = Reports;
})(this);
