/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:13 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

	var periods		=  [
							{value:"yesterday",descr:"Yesterday"},
							{value:"currWeek",descr:"Current Week"},
							{value:"prevWeek",descr:"Previous Week"},
							{value:"currMonth",descr:"Current Month"},
							{value:"prevMonth",descr:"Previous Month"}
						];

    var Report_Flow = can.Control({
        // Default options
        defaults : {
            view				: "views/report_flow.ejs"
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options));
        },
        show: function(){
			this.options.menusMode.load();
            //console.log(this.options.credentials);
            this.element.html(can.view("views/report_flow.ejs", {data:[{nodes:this.options.nodes,periods:periods}]}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#report_flow").addClass("active");

			$("select.multiselect").multiSelect({showHeader:false});
			$(".ui-multiselect").attr("style", "width:226px;");
        },
        hide: function(){
            this.element.slideUp(200);
        },
        "{document} #report_flow click": function(){
            //if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	{
					that.options.menusMode.clearTimer();
					$.when(Models.Report_Flow.findAll().then(function (findAllResponse) {
						that.options.nodes = findAllResponse;
						that.show();
					}));
				}
			}));
            //}
        },
        "{document} .graph-flow click": function(){
			var selected_nodes = "";
			var selected_period = "";
			var selected_view = "";

			var data = $("#report-form").serialize().split("&");

			for (var i = 0; i < data.length; i++)	{
				var entry = data[i].split("=");

				if (entry[0] == "node")				selected_nodes += ""+entry[1]+"__";
				else if (entry[0] == "period")	selected_period += ""+entry[1];
			}

			if (selected_nodes.length > 0)	selected_nodes = selected_nodes.substring(0, (selected_nodes.length-2));

			if (selected_nodes.length > 0)	{
				$.when(Models.Report_Flow.create({"nodes":selected_nodes,"period": selected_period}).then(function (response) {
					$("div#result_div").html("");

					Theme.init ();

					selected_nodes = selected_nodes.split(/__/);

					var innerHTML = "<center>";

						innerHTML += "<div class='graph-heading'>Cumulative Flow</div>";
						innerHTML += "<br/>";
						innerHTML += "<div id='cumulative_flow'>Loading...</div>";

					innerHTML += "</center>";
					innerHTML += "<br/>";

					$("div#result_div").append(innerHTML);

					var data = [];

					$.each(selected_nodes,function(i,v){
						var dataset = [];

						if (is_object(response[v]["data"]))	{
							var resultSet = response[v]["data"];

							for( var i = 0; i < resultSet.length; i++ ) {
								var year = parseFloat(resultSet[i]["year"]);
								var month = parseFloat(resultSet[i]["month"]);
								var day = parseFloat(resultSet[i]["day"]);
								var hours = parseFloat(resultSet[i]["hours"]);
								var minutes = parseFloat(resultSet[i]["minutes"]);
								var seconds = parseFloat(resultSet[i]["seconds"]);
								var value = parseFloat(resultSet[i]["value"]).toFixed(3);

								dataset.push([new Date(year, month, day, hours, minutes, seconds), value]);
							}

							data.push({
												data: dataset,
												label: response[v]["label"],
												points: { show: false },
												lines: { lineWidth: 2, fill: false }
											});
						}
					});

					$("div#cumulative_flow").addClass("chart-holder");

					Charts.line ("#cumulative_flow", data);
					$("#cumulative_flow").UseTooltip();
				}));
			}
        }
    });

    namespace.Report_Flow = Report_Flow;
})(this);
