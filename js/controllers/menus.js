/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:13 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

	var constructor;
	var constructor_holder;
	var timer;

    var Menus = can.Control({
        // Default options
        defaults : {
            view : "views/menus.ejs",
			menus : ""
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options));
			constructor = this;
		},
        show: function(){
            //console.log(this.options.credentials);
			/*
			if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
				$("#menu").html(can.view(this.options.view, {data:this.options.menus}));
			}
			*/
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	$("#menu").html(can.view(that.options.view, {data:that.options.menus}));
			}));

        },
        hide: function(){
            $("#menu").html("");
        },
		load: function(){
			var that = this;
			$.when(Models.Menus.findAll().then(function (findAllResponse) {
				that.options.menus = findAllResponse;
				that.show();
			}));
		},
		startTimer: function(holder){
			constructor_holder = holder;

			timer = setTimeout(function(){
				constructor_holder.reload();
				constructor.startTimer(constructor_holder);
			}, 10000);
		},
		clearTimer:function(){
			clearTimeout(timer);
		}
    });

    namespace.Menus = Menus;
})(this);
