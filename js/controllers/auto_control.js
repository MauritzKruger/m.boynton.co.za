/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:12 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

    var Auto_Control = can.Control({
        // Default options
        defaults : {
            view	: "views/auto_control.ejs",
			nodes	: ""
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options))
        },
        show: function(){
			this.options.menusMode.load();
            //console.log(this.options.credentials);
            this.element.html(can.view(this.options.view, {data:this.options.nodes}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#auto_control").addClass("active");
        },
        hide: function(){
            this.element.slideUp(200);
        },
		showResponse: function(response, doRefresh){
			$("div.page-message").text(response["message"]).addClass(response["type"].toLowerCase()).show();
			setTimeout("$('div.page-message').hide()", 3000);

			if (doRefresh == true)	{
				var that = this;
				setTimeout(function(){
					$.when(Models.Auto_Control.findAll().then(function (findAllResponse) {
						that.options.nodes = findAllResponse;
						that.show();
					}))
				}, 2000);
			}
		},
		"{document} #auto_control click": function(){
            //if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	{
					that.options.menusMode.clearTimer();
					$.when(Models.Auto_Control.findAll().then(function (findAllResponse) {
						that.options.nodes = findAllResponse;
						that.show();
					}));
				}
			}));
            //}
		},
		"{document} .powerBtn click": function(el){
			var that = this;
			$.when(Models.Auto_Control.create({"func":"powerOnOff","id": "all","control": el.attr("id")}).then(function (response) {
				that.showResponse(response, true);
			}));
		},
		"{document} .power change": function(el){
			var that = this;
			$.when(Models.Auto_Control.create({"func":"powerOnOff","id": el.attr("id"),"control": el.attr("value")}).then(function (response) {
				that.showResponse(response);
			}));
		},
		"{document} .lock change": function(el){
			var control = (el.is(":checked")) ? 1 : 0;
			var element = el.attr("id");
			var that = this;
			$.when(Models.Auto_Control.create({"func":"lockOnOff","id": el.attr("id"),"control": control}).then(function (response) {
				that.showResponse(response, true);
			}));
		}
    });

    namespace.Auto_Control = Auto_Control;
})(this);

