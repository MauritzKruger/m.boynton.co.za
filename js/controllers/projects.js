/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:12 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

    var Projects = can.Control({
        // Default options
        defaults : {
            view	: "views/projects.ejs",
			data	: "",
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options));
        },
        show: function(){
			this.options.menusMode.load();
            //console.log(this.options.credentials);
            this.element.html(can.view("views/projects.ejs", {data:this.options.data}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#projects").addClass("active");
        },
        hide: function(){
            this.element.slideUp(200);
        },
		getData: function(){
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	{
					that.options.menusMode.clearTimer();
					that.options.data = new Array();
					$.when(Models.Projects.findAll().then(function (findAllResponse) {
						var a = 0;
						var office_count = findAllResponse["offices"].length;
						if (is_object(findAllResponse["offices"]))	{
							$.each(findAllResponse["offices"],function(i, v)	{
								$.when(Models.Projects.findOne({"officeID":v["id"]}).then(function (findOneResponse) {
									var project_count = findOneResponse["projects"].length;
									var inner = new Array();
									var b = 0;
									if (is_object(findOneResponse["projects"]))	{
										$.each(findOneResponse["projects"],function(j, w)	{
											inner[b++] = [w["id"],w["name"],w["selected"]];
										});
										that.options.data[a++] = [v["id"],v["name"],inner];
										if (a == office_count && b == project_count)	that.show();
									}
								}));
							});
						}
					}));
				}
			}));
		},
        "{document} #projects click": function(){
            //if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
			this.getData();
            //}
        },
        "{document} .office-project click": function(el){
			var that = this;
			$.when(Models.Projects.create({"officeID":el.attr("id"),"projectID": el.attr("name")}).then(function (response) {
				that.options.statusMode.getData();
			}));
		}

    });

    namespace.Projects = Projects;
})(this);
