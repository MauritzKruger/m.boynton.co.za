/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

    var Signin = can.Control({
        // Default options
        defaults : {
            view : "views/signin.ejs"
        }
    }, {
        // Initialize
        init: function() {
            this.show("");
        },
        show: function(response){
            this.element.html(can.view(this.options.view, {}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#home").addClass("active");

			var usrn = $.cookie("username");
			var psswd = $.cookie("password");

			$("#username").val(usrn);
			$("#password").val(psswd);

			this.showResponse(response);
        },
        hide: function(){
            this.element.slideUp(200);
        },
		showResponse: function(response){
			if (is_object(response["message"]))	{
				$("div.page-message").text(response["message"]).addClass(response["type"].toLowerCase()).show();
				setTimeout("$('div.page-message').hide()", 3000);
			}
		},
        "{document} #home click": function(){
            this.show();
        },
        "{document} #password focusout": function(el,e){
            var psswd = el[0].value;
            //console.log(psswd);
            this.options.credentials.attr({"password":psswd});
            //console.log(this.options.credentials);
            this.options.credentials
        },
        "{document} #username focusout": function(el,e){
            var usrn = el[0].value;
            //console.log(usrn);
            this.options.credentials.attr({"username":usrn});
            //console.log(this.options.credentials);
            this.options.credentials
        },
        "{document} #username keypress": function(el,e){
			if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
				var usrn = $("#username").val();
				var psswd = $("#password").val();
				//console.log(psswd);
				this.options.credentials.attr({"username":usrn});
				this.options.credentials.attr({"password":psswd});
				//console.log(this.options.credentials);
				this.options.credentials;

				//console.log(this.options.credentials);
				/*
				if(this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
					this.options.projectsMode.show();
				}
				*/
				var that = this;
				$.when(Models.Signin.create({"email":this.options.credentials.attr("username") ,"password": this.options.credentials.attr("password")}).then(function (response) {
					if (response["loggedIn"])	{
						$.cookie("username", that.options.credentials.attr("username"), { expires: 365 });
						$.cookie("password", that.options.credentials.attr("password"), { expires: 365 });

						eval("that.options."+response["redirectTo"]+"."+response["func"]+"();");
					}
					else	{
						that.show(response);
					}
				}));
			}
		},
        "{document} #password keypress": function(el,e){
			if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
				var usrn = $("#username").val();
				var psswd = $("#password").val();
				//console.log(psswd);
				this.options.credentials.attr({"username":usrn});
				this.options.credentials.attr({"password":psswd});
				//console.log(this.options.credentials);
				this.options.credentials;

				//console.log(this.options.credentials);
				/*
				if(this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
					this.options.projectsMode.show();
				}
				*/
				var that = this;
				$.when(Models.Signin.create({"email":this.options.credentials.attr("username") ,"password": this.options.credentials.attr("password")}).then(function (response) {
					if (response["loggedIn"])	{
						$.cookie("username", that.options.credentials.attr("username"), { expires: 365 });
						$.cookie("password", that.options.credentials.attr("password"), { expires: 365 });

						eval("that.options."+response["redirectTo"]+"."+response["func"]+"();");
					}
					else	{
						that.show(response);
					}
				}));
			}
		},
        "{document} #signin click": function(){
			var usrn = $("#username").val();
			var psswd = $("#password").val();
			//console.log(psswd);
			this.options.credentials.attr({"username":usrn});
			this.options.credentials.attr({"password":psswd});
			//console.log(this.options.credentials);
			this.options.credentials;

            //console.log(this.options.credentials);
            /*
			if(this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
				this.options.projectsMode.show();
			}
			*/
			var that = this;
			$.when(Models.Signin.create({"email":this.options.credentials.attr("username") ,"password": this.options.credentials.attr("password")}).then(function (response) {
				if (response["loggedIn"])	{
					$.cookie("username", that.options.credentials.attr("username"), { expires: 365 });
					$.cookie("password", that.options.credentials.attr("password"), { expires: 365 });

					eval("that.options."+response["redirectTo"]+"."+response["func"]+"();");
				}
				else	{
					that.show(response);
				}
			}));
        },
        "{document} #signout click": function(){
            //console.log(this.options.credentials);
			var that = this;
			$.when(Models.Signin.destroy().then(function (response) {
				that.options.credentials.attr("username","");
				that.options.credentials.attr("password","");
				that.options.menusMode.clearTimer();
				that.options.menusMode.hide();
				that.show(response);
			}));
        }

    });

    namespace.Signin = Signin;
})(this);

