/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:12 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

    var Detail = can.Control({
        // Default options
        defaults : {
            view	: "views/detail.ejs",
			node	: "",
			nodeID	: ""
        }
    }, {
        // Initialize
        init: function() {
			//this.element.append(can.view(this.options.view, this.options))
        },
        show: function(){
			this.options.menusMode.load();
			//console.log(this.options.credentials);
            this.element.html(can.view(this.options.view, {data:this.options.node}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#detail").addClass("active");
        },
        hide: function(){
            this.element.slideUp(200);
        },
		showResponse: function(response){
			$("div.page-message").text(response["message"]).addClass(response["type"].toLowerCase()).show();
			setTimeout("$('div.page-message').hide()", 3000);
		},
		reload: function(){
			var that = this;
			$.when(Models.Detail.findOne({"id": this.options.nodeID}).then(function (findOneResponse) {
				that.options.node = findOneResponse;
				that.show();
				nodeDescr = findOneResponse["nodeName"];
				$.each(findOneResponse["nodeGauges"],function(i, v)	{
					drawVisualization(nodeDescr,v);
					//$("#gauge_"+v["id"]).cirque({value: v["value"], total: v["upper"], arcColor: "#FF9900", descr: v["name"], label: "label"});
				});
			}));
		},
		"{document} .nodeDetail click": function(el){
			this.options.menusMode.clearTimer();
			this.options.nodeID = el.attr("id");

			var that = this;
			this.options.menusMode.startTimer(that);

			$.when(Models.Detail.findOne({"id": this.options.nodeID}).then(function (findOneResponse) {
				that.options.node = findOneResponse;
				that.show();
				nodeDescr = findOneResponse["nodeName"];
				$.each(findOneResponse["nodeGauges"],function(i, v)	{
					drawVisualization(nodeDescr,v);
					//$("#gauge_"+v["id"]).cirque({value: v["value"], total: v["upper"], arcColor: "#FF9900", descr: v["name"], label: "label"});
				});
			}));
		},
		"{document} .power change": function(el){
			var that = this;
			$.when(Models.Detail.create({"func":"powerOnOff","id": el.attr("id"),"control": el.attr("value")}).then(function (response) {
				that.showResponse(response);
			}));
		},
		"{document} .untrip click": function(el){
			var that = this;
			$.when(Models.Detail.create({"func":"unTrip","id": el.attr("id"),"control": "untrip"}).then(function (response) {
				el.attr("disabled", "disabled");
				that.showResponse(response);
			}));
		}
    });

    namespace.Detail = Detail;
})(this);

