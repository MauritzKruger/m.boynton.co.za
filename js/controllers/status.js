/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:13 PM
 */

/*global can, Models, $ */
(function (namespace, undefined) {
    "use strict";

    var Status = can.Control({
        // Default options
        defaults : {
            view : "views/status.ejs",
			nodes : "",
        }
    }, {
        // Initialize
        init: function() {
            //this.element.append(can.view(this.options.view, this.options));
        },
        show: function(){
			this.options.menusMode.load();
            //console.log(this.options.credentials);
            this.element.html(can.view(this.options.view, {data:this.options.nodes}));
            this.element.slideDown(200);
            $("#menu ul li").removeClass("active");
            $("#status").addClass("active");
        },
        hide: function(){
            this.element.slideUp(200);
        },
		reload: function(){
			var that = this;
			$.when(Models.Status.findAll().then(function (findAllResponse) {
				that.options.nodes = findAllResponse;
				that.show();
			}));
		},
		getData: function()	{
			var that = this;
			$.when(Models.Menus.findOne().then(function (response) {
				if (response)	{
					that.options.menusMode.clearTimer();
					that.options.menusMode.startTimer(that);
					$.when(Models.Status.findAll().then(function (findAllResponse) {
						that.options.nodes = findAllResponse;
						that.show();
					}));
				}
			}));
		},
        "{document} #status click": function(){
            //if (this.options.credentials.attr("username") === this.options.secret.attr("username") && this.options.credentials.attr("password") === this.options.secret.attr("password")){
			this.getData();
            //}
        }
    });

    namespace.Status = Status;
})(this);
