(function() {
    $(function() {
        // Set up a route that maps to the `filter` attribute
        can.route( ":filter" );
        // Delay routing until we initialized everything
        can.route.ready(false);

        var Credentials = new can.Observe({username:"",password:""});

        // Initialize the app
        var menusMode = new Menus("#application", {"credentials":Credentials});
        var statusMode = new Status("#application", {"credentials":Credentials,"menusMode":menusMode});
        var projectsMode = new Projects("#application", {"credentials":Credentials,"statusMode":statusMode,"menusMode":menusMode});
        var detailMode = new Detail("#application", {"credentials":Credentials,"menusMode":menusMode});
        var reportsMode = new Reports("#application", {"credentials":Credentials,"menusMode":menusMode});
        var reportFlowMode = new Report_Flow("#application", {"credentials":Credentials,"menusMode":menusMode});
        var auto_controlMode = new Auto_Control("#application", {"credentials":Credentials,"menusMode":menusMode});
        var signinMode = new Signin("#application", {"credentials":Credentials,"projectsMode":projectsMode,"statusMode":statusMode,"menusMode":menusMode});

        // Now we can start routing
        can.route.ready(true);
    });
})();
