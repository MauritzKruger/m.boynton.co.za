google.load("visualization", "1", {packages:["gauge","corechart"]});

var nodeDescr = "";

function drawVisualization(name, info) {
	//$("#gauge_"+v["id"]).cirque({value: v["value"], total: v["upper"], arcColor: "#FF9900", descr: v["name"], label: "label"});
	// Create and populate the data table.
	var min;var max;

	if (info["lower"] < 0)	min = info["lower"]*120/100;	else min = info["lower"]*80/100;
	if (info["upper"] > 0)	max = info["upper"]*120/100;	else max = info["upper"]*80/100;

	// Create and draw the visualization.
	if (info["name"] == "Water Level [m]")	{
		if (name == "FF6_Main")	{
			var data = google.visualization.arrayToDataTable([
				['Label', 'Value'],
				["", info["value"]]
			]);

			new google.visualization.ColumnChart(document.getElementById("gauge_"+info["id"])).draw(data, {width: 150, height: 150, backgroundColor: 'transparent',
					vAxis: {minValue: min, maxValue: max, baselineColor: '#000', gridlines: {color: '#000'}, textStyle: {color: '#000'}, viewWindowMode: 'explicit', viewWindow: {max: max, min: min}},
					legend: {position: 'none'}, series: [{color: 'cornflowerblue'}]}
			);

			document.getElementById("gauge_"+info["id"]+"_extra").innerHTML = "Total Tank Volume: 20000<i>l</i><br/>Volume left in tank: " + ((info["value"] / 2.4) * 18246).toFixed(0) + "<i>l</i>";
		} else	{
			if (max < 0)	max = 0;

			var reading = info["value"];

			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Label');
			data.addColumn('number', '');
			data.addColumn('number', '');
			data.addColumn({type:'string', role:'tooltip'});
			data.addRow(["",reading,(min - reading),"Water Level [m]: "+reading]);

			new google.visualization.ColumnChart(document.getElementById("gauge_"+info["id"])).draw(data, {width: 150, height: 150, backgroundColor: 'transparent', isStacked: true,
				vAxis: {minValue: min, maxValue: max, baselineColor: '#000', gridlines: {color: '#000'}, textStyle: {color: '#000'}, viewWindowMode: 'explicit', viewWindow: {max: max, min: min}},
				tooltip: {textStyle: {color: 'black', fontSize: 10}, position: 'top'},
				legend: {position: 'none'}, series: [{color: 'transparent'}, {color: 'cornflowerblue'}]}
			);
		}
	} else	{
		var data = google.visualization.arrayToDataTable([
			['Label', 'Value'],
			["", info["value"]]
		]);

		if (min < 0 && info["value"] < 0)
			new google.visualization.Gauge(document.getElementById("gauge_"+info["id"])).draw(data, {width: 150, height: 150,
				min: min, max: max,
				redFrom: min, redTo: info["lower"],
				greenFrom: info["lower"], greenTo: info["upper"],
				yellowFrom: info["upper"], yellowTo: max,
				minorTicks: 10}
			);
		else
			new google.visualization.Gauge(document.getElementById("gauge_"+info["id"])).draw(data, {width: 150, height: 150,
				min: min, max: max,
				yellowFrom: min, yellowTo: info["lower"],
				greenFrom: info["lower"], greenTo: info["upper"],
				redFrom: info["upper"], redTo: max,
				minorTicks: 10}
			);
	}
}
