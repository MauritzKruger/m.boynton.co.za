/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:23 PM
 */

/*global can */
(function (namespace, undefined) {
    "use strict";

    //Model
    var Auto_Control = can.Model({
        findAll	:"GET ././api/auto_control_nodes",
		create	: "POST ././api/runFunction/{func}/{id}/{control}"
    }, {});

    // List
    Auto_Control.List = can.Model.List({

    });

    namespace.Models = namespace.Models || {};
    namespace.Models.Auto_Control = Auto_Control;
})(this);