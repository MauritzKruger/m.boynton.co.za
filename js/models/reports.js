/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:23 PM
 */

/*global can */
(function (namespace, undefined) {
    "use strict";

    //Model
    var Reports = can.Model({
        findAll	: "GET ././api/report_nodes",
        findOne	: "GET ././api/report_parameters/{nodes}",
        create	: "POST ././api/report_data/{nodes}/{params}/{period}"
    }, {});

    // List
    Reports.List = can.Model.List({

    });

    namespace.Models = namespace.Models || {};
    namespace.Models.Reports = Reports;
})(this);