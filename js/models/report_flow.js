/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:23 PM
 */

/*global can */
(function (namespace, undefined) {
    "use strict";

    //Model
    var Report_Flow = can.Model({
        findAll	: "GET ././api/report_flow_nodes",
        create	: "POST ././api/report_flow_data/{nodes}/{period}"
    }, {});

    // List
    Report_Flow.List = can.Model.List({

    });

    namespace.Models = namespace.Models || {};
    namespace.Models.Report_Flow = Report_Flow;
})(this);