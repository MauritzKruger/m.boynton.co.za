/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:24 PM
 */

/*global can */
(function (namespace, undefined) {
    "use strict";

    //Model
    var Menus = can.Model({
        findAll:"GET ././api/menus",
        findOne	: "GET ././api/login"
    }, {});

    // List
    Menus.List = can.Model.List({

    });

    namespace.Models = namespace.Models || {};
    namespace.Models.Menus = Menus;
})(this);