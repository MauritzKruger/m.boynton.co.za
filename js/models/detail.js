/**
 * Created with JetBrains PhpStorm.
 * User: nomad
 * Date: 2013/01/08
 * Time: 12:23 PM
 */

/*global can */
(function (namespace, undefined) {
    "use strict";

    //Model
    var Detail = can.Model({
        findOne	: "GET ././api/nodes/{id}",
		create	: "POST ././api/runFunction/{func}/{id}/{control}"
    }, {});

    // List
    Detail.List = can.Model.List({

    });

    namespace.Models = namespace.Models || {};
    namespace.Models.Detail = Detail;
})(this);