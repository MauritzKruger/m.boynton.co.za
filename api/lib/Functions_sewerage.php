<?php
class Functions_sewerage	{
/*
	Badge Colors
	============
	badge					:= Functions_sewerage::GREY
	badge badge-success		:= Functions_sewerage::GREEN
    badge badge-warning		:= yellow/orange
    badge badge-important	:= Functions_sewerage::RED
    badge badge-info		:= Functions_sewerage::BLUE
    badge badge-inverse		:= black
*/

	const RED		= "badge-important";
	const GREEN	= "badge-success";
	const BLUE	= "badge-info";
	const GREY	= "";

    var $dbo;

    function Functions_sewerage()	{
        $this->dbo = new Database();
    }

	function getInfoHeadings()	{
		return array(
								"RX/TX",
								"Constraint<br/>Violation",
								"Control<br/>Status",
								"Pump<br/>On",
								"Pump<br/>Status",
								"Grid<br/>Power",
								"Security<br/>Violation"
							);
	}

	function setVariable($var = "", $val = "")	{
		$_SESSION[$var] = $val;
	}

	function getVariable($var)	{
		return $_SESSION[$var];
	}

	function getParameterID($nodeID, $parameterName)	{
		$projectID = $this->getVariable("projectID");

		$sql = "SELECT id FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND name = '".$parameterName."'";

		return $this->dbo->q($sql);
	}

    function getValue($nodeID, $parameterID)	{
		$projectID = $this->getVariable("projectID");

		//$sql = "SELECT id, value FROM sample WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND parameterID = '".$parameterID."' ORDER BY tstamp DESC LIMIT 1";
		$sql = "SELECT value FROM sample WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND parameterID = '".$parameterID."' ORDER BY tstamp DESC LIMIT 1";

		return $this->dbo->q($sql);
    }

    function getTimeStamp($nodeID)	{
		$projectID = $this->getVariable("projectID");

		$sql = "SELECT tstamp FROM sample ".
					"WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND parameterID IN (SELECT id FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND active = '1') ".
					"ORDER BY tstamp DESC LIMIT 1";

		return $this->dbo->q($sql);
    }

    function checkBit($nodeID, $parameter, $bit)	{
        $byte = (int)$this->getValue($nodeID, $this->getParameterID($nodeID, $parameter));

        if ($byte < 0)  $bin = decbin(256 + $byte);
        else            $bin = decbin($byte);

        $bin = substr("00000000", 0, 8 - strlen($bin)) . $bin;

        return ($bin[$bit]) ? Functions_sewerage::GREY : Functions_sewerage::GREEN;
    }

    function checkBooster($nodeID, $parameter, $status)	{
		$projectID = $this->getVariable("projectID");

        $value = $this->getValue($nodeID, $this->getParameterID($nodeID, $parameter));

        if ($status == "ACTIVE")
            return ($value == 1) ? Functions_sewerage::GREEN : Functions_sewerage::GREY;
        else
            return ($value == 1) ? Functions_sewerage::RED : Functions_sewerage::GREY;
	}

    function checkRXTX($nodeID)	{
        $lastTimeStamp = $this->getTimeStamp($nodeID);

		$sql = "SELECT commsThreshold FROM Node WHERE id = '".$nodeID."'";

		$threshold = $this->dbo->q($sql);

		$now = date("Y-m-d H:i:s", strtotime("-".$threshold." seconds"));
		return ($lastTimeStamp > $now) ? Functions_sewerage::GREEN : Functions_sewerage::RED;
    }

    function checkConstraints($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $sql = "SELECT id, name, upper, lower FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND active = '1' AND display = '1'";

		$parameters = $this->dbo->q($sql);

        if (is_array($parameters))	{
            foreach ($parameters as $p)	{
                $currentValue = $this->getValue($nodeID, $p[0]);

                if ($currentValue == "-")	return Functions_sewerage::RED;

                if ($currentValue > $p[2])			return Functions_sewerage::RED;
                else if ($currentValue < $p[3])	return Functions_sewerage::RED;
            }
        }

        $vsl = "SELECT id, name, upper, lower FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND name = 'Virtual_Sump_Level' LIMIT 1";

		$vsl = $this->dbo->q($vsl);

		if (is_array($vsl))	{
			$currentValue = $this->getValue($nodeID, $vsl[0]);

			if ($currentValue > $vsl[2])		return Functions_sewerage::RED;
			else if ($currentValue < $vsl[3])	return Functions_sewerage::RED;
		}

        return Functions_sewerage::GREEN;
    }

    function checkControlStatus($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $value = $this->getValue($nodeID, $this->getParameterID($nodeID, "Control_Status"));

		return ($value == 1) ? Functions_sewerage::GREY : Functions_sewerage::GREEN;
	}

	function checkPumping($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $value1 = $this->getValue($nodeID, $this->getParameterID($nodeID, "Booster_1_Active"));
        $value2 = $this->getValue($nodeID, $this->getParameterID($nodeID, "Booster_2_Active"));

		return ($value1 == 1 || $value2 == 1) ? Functions_sewerage::GREEN : Functions_sewerage::GREY;
	}

    function checkStatus($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $tripped = array("Booster_1_Tripped","Booster_2_Tripped","Booster_3_Tripped","Sump_Tripped");

        if (is_array($tripped))   {
            foreach ($tripped as $t)    {
                $value = $this->getValue($nodeID, $this->getParameterID($nodeID, $t));

                if ($value == 1)	return Functions_sewerage::RED;
            }
        }

		return Functions_sewerage::GREEN;
    }

	function checkGridPower($nodeID)	{
		$projectID = $this->getVariable("projectID");
		$nodeDisplayName = $this->dbo->q("SELECT displayName FROM Node WHERE id = '".$nodeID."'");

		$value = $this->getValue($nodeID, $this->getParameterID($nodeID, "Phase_Failure"));

		if ($value == "-")	return Functions_sewerage::RED;

		if (in_array($nodeDisplayName, array("Pump Station C","Pump Station D","Pump Station E"),true))
			return ($value == 0) ? Functions_sewerage::GREEN : Functions_sewerage::RED;
		else
			return ($value == 1) ? Functions_sewerage::GREEN : Functions_sewerage::RED;
	}

	function checkSecurity($nodeID)	{
		$projectID = $this->getVariable("projectID");

		$value = $this->getValue($nodeID, $this->getParameterID($nodeID, "Security_Violation"));

		if ($value == "-")	return Functions_sewerage::GREY;

		return ($value == 1) ? Functions_sewerage::RED : Functions_sewerage::GREY;
	}

    function checkSumpLevel($nodeID, $parameter)	{
		$projectID = $this->getVariable("projectID");

		$value = $this->getValue($nodeID, $this->getParameterID($nodeID, $parameter));

		if ($value == "-")	return Functions_sewerage::GREY;

		return ($value == 1) ? Functions_sewerage::GREEN : Functions_sewerage::GREY;
	}

	function getNodeGauges($node)	{
		$projectID = $this->getVariable("projectID");
		$nodeID = $node[0];

		$gauges = array();

		$parameters = $this->dbo->q("SELECT id, name, upper, lower, displayName FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND active = '1' AND display = '1' ".
									"ORDER BY displayName");

		if (is_array($parameters))	{
			foreach($parameters as $p)	{
				$alert = "";

                $values = $this->dbo->q("SELECT s.value, p.display, p.id FROM (sample AS s INNER JOIN Parameter AS p ON s.parameterID = p.id) ".
										"WHERE s.projectID = '".$projectID."' AND p.nodeID = '".$nodeID."' AND p.id = '".$p[0]."' ORDER BY s.tstamp DESC LIMIT 1");

				if ($this->checkRXTX($nodeID) != Functions_sewerage::RED)	{
					if (is_array($values))	{
						foreach($values as $v)	{
							if ($v[0] > $p[2])	$alert = "Upper constraint of ".$p[2]." violated";
							if ($v[0] < $p[3])	$alert = "Lower constraint of ".$p[2]." violated";

							$gauges[] = array(
												"name" => $p[4],
												"value" => (($v[1] == "1") ? floatval(round($v[0], 2)) : 0),
												"upper" => (($p[2] != "") ? floatval(round($p[2], 2)) : 0),
												"lower" => (($p[3] != "") ? floatval(round($p[3], 2)) : 0),
												"id" => $p[0],
												"alert" => $alert
											);
						}
					}
					else	{
						$gauges[] = array(
											"name" => $p[4],
											"value" => 0,
											"upper" => (($p[2] != "") ? floatval(round($p[2], 2)) : 0),
											"lower" => (($p[3] != "") ? floatval(round($p[3], 2)) : 0),
											"id" => $p[0],
											"alert" => $alert
										);
					}
				} else	{
					$gauges[] = array(
										"name" => $p[4],
										"value" => 0,
										"upper" => 1,
										"lower" => 0,
										"id" => $p[0],
										"alert" => $alert
									);
				}
            }
		}

		return $gauges;
	}

	function getNodeIndicators($node)	{
		$projectID = $this->getVariable("projectID");
		$nodeID = $node[0];

		$nodeIndicators = array();

		$ledParameters = array(
												"Phase_Failure","Security_Violation",
												"Booster_1_Active","Booster_1_Tripped","Booster_2_Active","Booster_2_Tripped","Booster_3_Active","Booster_3_Tripped",
												"Sump_Active","Sump_Tripped","Waste_Screen_Active","Waste_Screen_Tripped","Sump_Level_1","Sump_Level_2","Sump_Level_3"
											);
		$ledDescriptions = array(
												"Grid Power","Security Violation",
												"Pump 1 On","Pump 1 Tripped","Pump 2 On","Pump 2 Tripped","Pump 3 On","Pump 3 Tripped",
												"Sump Pump On","Sump Pump Tripped","Waste Screen On","Waste Screen Tripped","Probe 1","Probe 2","Probe 3"
											);

		$title = "- Status Indicators -";
		$descr = "";

		$nodeIndicators["status"]["title"] = $title;
		$nodeIndicators["status"]["descr"] = $descr;

		$rxtx = $this->checkRXTX($nodeID);

		for ($i = 0; $i < count($ledParameters); $i++)	{
			$parameterID = $this->getParameterID($nodeID, $ledParameters[$i]);

			if ($parameterID > 0 && is_numeric($parameterID))	{
				switch ($ledParameters[$i])	{
					case "Phase_Failure":	{
						$class = ($rxtx != Functions_sewerage::RED) ? $this->checkGridPower($nodeID) : Functions_sewerage::GREY;
						break;
					}
					case "Security_Violation":	{
						$class = ($rxtx != Functions_sewerage::RED) ? $this->checkSecurity($nodeID) : Functions_sewerage::GREY;
						break;
					}
					case "Booster_1_Active":
					case "Booster_2_Active":
					case "Booster_3_Active":
					case "Sump_Active":
					case "Waste_Screen_Active":	{
						$class = ($rxtx != Functions_sewerage::RED) ? $this->checkBooster($nodeID, $ledParameters[$i], "ACTIVE") : Functions_sewerage::GREY;
						break;
					}
					case "Booster_1_Tripped":
					case "Booster_2_Tripped":
					case "Booster_3_Tripped":
					case "Sump_Tripped":
					case "Waste_Screen_Tripped":	{
						$class = ($rxtx != Functions_sewerage::RED) ? $this->checkBooster($nodeID, $ledParameters[$i], "TRIPPED") : Functions_sewerage::GREY;
						break;
					}
					case "Sump_Level_1":
					case "Sump_Level_2":
					case "Sump_Level_3":  {
						$class = ($rxtx != Functions_sewerage::RED) ? $this->checkSumpLevel($nodeID, $ledParameters[$i]) : Functions_sewerage::GREY;
						break;
					}
					default:
						break;
				}

				$nodeIndicators["status"]["data"][] = array("class" => $class, "descr" => $ledDescriptions[$i]);
			}
		}

		return $nodeIndicators;
	}

	function getNodeControls($node)	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->getVariable("projectID");
		$usertypeID = $this->getVariable("usertypeID");
		$nodeID = $node[0];

		$controls = array();

		return $controls;
	}

	function getTimers()	{
		$projectID = $this->getVariable("projectID");

		$timers = array("oscillationTimer","overCapacityTimer","recoveryTimer");
		$timerData = array();

		if (is_array($timers))	{
			foreach ($timers as $timer)	{
				unset($value);

				if ($timer == "oscillationTimer")	{
					$sql = "SELECT id FROM Node WHERE projectID = '".$projectID."' AND displayName = 'Ifafi'";

					$nodeID = $this->dbo->q($sql);

					$value = $this->getValue($nodeID, $this->getParameterID($nodeID, "Oscillation_Timer"));
				} else	{
					$sql = "SELECT id FROM Node WHERE projectID = '".$projectID."' AND displayName = 'Rietfontein Inlet'";

					$nodeID = $this->dbo->q($sql);

					$parameterName = ($timer == "overCapacityTimer") ? "Over_Capacity_Timer" : "Recovery_Timer";

					$value = $this->getValue($nodeID, $this->getParameterID($nodeID, $parameterName));
				}

				$value = ($value == 1) ? Functions_sewerage::GREEN : Functions_sewerage::GREY;

				$timerData[$timer] = $value;
			}
		}

		return $timerData;
	}

	function getNodeDetails($node)	{
		$projectID = $this->getVariable("projectID");
		$tstamp = date("Y-m-d");

		$headingTypes["PLC"] = array("rxtx","constraints","controlStatus","pumping","status","gridPower","security");
		$headings = $headingTypes[$node[2]];

		$index = 0;
		$tmpArr = array();
		$tmpArr[$index++] = count($headings) + 2;
		$tmpArr[$index++] = $node[0];
		$tmpArr[$index++] = $node[1];

		if (is_array($headings))	{
			foreach ($headings as $h)	{
				switch ($h)	{
					case "rxtx":	{
						$tmpArr[$index++] = array("class" => $this->checkRXTX($node[0]), "val" => "");
						break;
					}
					case "constraints":	{
						$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkConstraints($node[0]) : Functions_sewerage::GREY;

						$tmpArr[$index++] = array("class" => $value, "val" => "");
						break;
					}
					case "controlStatus":	{

						if ($node[1] == "Ifafi" || $node[1] == "Venice" || $node[1] == "Xanadu")	{
							$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkControlStatus($node[0]) : Functions_sewerage::GREY;

							$tmpArr[$index++] = array("class" => $value, "val" => (($value != Functions_sewerage::RED) ? $this->getValue($node[0], $this->getParameterID($node[0], "Priority")) : ""));
						}
						else
							$tmpArr[$index++] = "";

						break;
					}
					case "pumping":	{
						if ($node[1] != "Rietfontein Inlet" && $node[1] != "Rietfontein Outlet")	{
							$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkPumping($node[0]) : Functions_sewerage::GREY;

							$tmpArr[$index++] = array("class" => $value, "val" => "");
						} else
							$tmpArr[$index++] = "";
						break;
					}
					case "status":	{
						if ($node[1] != "Rietfontein Inlet" && $node[1] != "Rietfontein Outlet")	{
							$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkStatus($node[0]) : Functions_sewerage::GREY;

							$tmpArr[$index++] = array("class" => $value, "val" => "");
						} else
							$tmpArr[$index++] = "";
						break;
					}
					case "gridPower":	{
						$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkGridPower($node[0]) : Functions_sewerage::GREY;

						$tmpArr[$index++] = array("class" => $value, "val" => "");
						break;
					}
					case "security":	{
						$value = ($this->checkRXTX($node[0]) != Functions_sewerage::RED) ? $this->checkSecurity($node[0]) : Functions_sewerage::GREY;

						$tmpArr[$index++] = array("class" => $value, "val" => "");
						break;
					}
					default:
						break;
				}
			}
		}

		return $tmpArr;
	}
}