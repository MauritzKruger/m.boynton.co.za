<?php
class Functions_boynton	{
/*
	Badge Colors
	============
	badge					:= Functions_boynton::GREY
	badge badge-success		:= Functions_boynton::GREEN
    badge badge-warning		:= yellow/orange
    badge badge-important	:= Functions_boynton::RED
    badge badge-info		:= Functions_boynton::BLUE
    badge badge-inverse		:= black
*/

	const RED		= "badge-important";
	const GREEN	= "badge-success";
	const BLUE	= "badge-info";
	const GREY	= "";

    var $dbo;

    function Functions_boynton()	{
        $this->dbo = new Database();
    }

	function getInfoHeadings()	{
		return array(
								"RX/TX",
								"Pump<br/>Status",
								"Pump<br/>Tripped",
								"Constraint<br/>Violation",
								"Volume<br/>Pumped [m<sup>3</sup>]"
							);
	}

	function setVariable($var = "", $val = "")	{
		$_SESSION[$var] = $val;
	}

	function getVariable($var)	{
		return $_SESSION[$var];
	}

	function getParameterID($nodeID, $parameterName)	{
		$projectID = $this->getVariable("projectID");

		$sql = "SELECT id FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND name = '".$parameterName."'";

		return $this->dbo->q($sql);
	}

    function getValue($nodeID, $parameterID)	{
		$projectID = $this->getVariable("projectID");

		//$sql = "SELECT id, value FROM sample WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND parameterID = '".$parameterID."' ORDER BY tstamp DESC LIMIT 1";
		$sql = "SELECT value FROM sample WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND parameterID = '".$parameterID."' ORDER BY tstamp DESC LIMIT 1";

		return $this->dbo->q($sql);
    }

    function getTimeStamp($nodeID)	{
		$projectID = $this->getVariable("projectID");

		$sql = "SELECT tstamp FROM sample WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' ORDER BY tstamp DESC LIMIT 1";

		return $this->dbo->q($sql);
    }

    function getTotalDailyVolume()	{
		$tstamp = date("Y-m-d");
		$projectID = $this->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, nt.name FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
				"WHERE n.projectID = '".$projectID."' AND n.displayName != 'Main_RES' AND n.active = '1' ORDER BY nt.id ASC, n.ranking ASC, n.displayName ASC";

		$nodes = $this->dbo->q($sql);

		$totalDailyVolume = 0;

		if (is_array($nodes) && count($nodes) > 0)	{
			foreach ($nodes as $node)       {
				if ($node[1] == "BH117" || $node[1] == "FF6_Main")	{
					$sql = "SELECT dailyFlow FROM DailyCumulativeFlow WHERE projectID = '".$projectID."' AND node = '".$node[0]."' AND tstamp = '".$tstamp."'";

					$totalDailyVolume += $this->dbo->q($sql);
				}
			}
		}

		$totalDailyVolume = number_format((double)$totalDailyVolume, 0, "", "");

		return $totalDailyVolume;
    }

    function checkBit($nodeID, $parameter, $bit)	{
        $byte = (int)$this->getValue($nodeID, $this->getParameterID($nodeID, $parameter));

        if ($byte < 0)  $bin = decbin(256 + $byte);
        else            $bin = decbin($byte);

        $bin = substr("00000000", 0, 8 - strlen($bin)) . $bin;

        if ($parameter == "DIGITAL_IN" || $parameter == "DIGITAL_OUT")  return (!$bin[$bit]) ? Functions_boynton::GREY : Functions_boynton::GREEN;

        return ($bin[$bit]) ? Functions_boynton::GREY : Functions_boynton::RED;
    }

    function checkRXTX($nodeID)	{
        $lastTimeStamp = $this->getTimeStamp($nodeID);

		$sql = "SELECT commsThreshold FROM Node WHERE id = '".$nodeID."'";

		$threshold = $this->dbo->q($sql);

		$now = date("Y-m-d H:i:s", strtotime("-".$threshold." seconds"));
		return ($lastTimeStamp > $now) ? Functions_boynton::GREEN : Functions_boynton::RED;
    }

    function checkStatus($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $cValue = $this->getValue($nodeID, $this->getParameterID($nodeID, "CURRENT"));
        $fValue = $this->getValue($nodeID, $this->getParameterID($nodeID, "FLOW"));
		$sql = "SELECT bit FROM NodeIndicator WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND (digitalOut = 'Borehole Pump Running' OR digitalOut = 'Booster Pump Running')";

		$bit = $this->dbo->q($sql);
		$bit = $bit[0];

        return (($cValue >= 3 || $fValue >= 3) && $this->checkBit($nodeID, "DIGITAL_OUT", ($bit - 1)) == Functions_boynton::GREEN) ? Functions_boynton::GREEN : Functions_boynton::GREY;
    }

    function checkTripped($nodeID)	{
        $byte = (int)$this->getValue($nodeID, $this->getParameterID($nodeID, "DIGITAL_OUT"));

        if ($byte < 0)  $bin = decbin(256 + $byte);
        else            $bin = decbin($byte);

        $bin = substr("00000000", 0, 8 - strlen($bin)) . $bin;

        return (!$bin[5]) ? Functions_boynton::GREY : Functions_boynton::RED;
    }

    function checkConstraints($nodeID)	{
		$projectID = $this->getVariable("projectID");

        $sql = "SELECT id, name, upper, lower FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND active = '1' AND display = '1'";

		$parameters = $this->dbo->q($sql);

        if (is_array($parameters) && count($parameters) > 0)	{
            foreach ($parameters as $p)	{
                $currentValue = $this->getValue($nodeID, $p[0]);

                if ($currentValue == "-")       return Functions_boynton::RED;

                if ($currentValue > $p[2])      return Functions_boynton::RED;
                else if ($currentValue < $p[3]) return Functions_boynton::RED;
            }
        }

        $vsl = "SELECT id, name, upper, lower FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND name = 'Virtual_Sump_Level' LIMIT 1";

		$vsl = $this->dbo->q($vsl);

        if (is_array($vsl) && count($vsl) > 0)	{
			$currentValue = $this->getValue($nodeID, $vsl[0]);

			if ($currentValue == "-")	return Functions_boynton::RED;

			if ($currentValue > $vsl[2])		return Functions_boynton::RED;
			else if ($currentValue < $vsl[3])	return Functions_boynton::RED;
		}

        return Functions_boynton::GREY;
    }

	function getNodeGauges($node)	{
		$projectID = $this->getVariable("projectID");
		$nodeID = $node[0];

		$gauges = array();

		$parameters = $this->dbo->q("SELECT id, name, upper, lower, displayName FROM Parameter WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND active = '1' AND display = '1' ".
									"ORDER BY displayName");

		if (is_array($parameters))	{
			foreach($parameters as $p)	{
				$alert = "";

                $values = $this->dbo->q("SELECT s.value, p.display, p.id FROM (sample AS s INNER JOIN Parameter AS p ON s.parameterID = p.id) ".
										"WHERE s.projectID = '".$projectID."' AND p.nodeID = '".$nodeID."' AND p.id = '".$p[0]."' ORDER BY s.tstamp DESC LIMIT 1");

				if ($this->checkRXTX($node[0]) != Functions_boynton::RED)	{
					if (is_array($values))	{
						foreach($values as $v)	{
							if ($v[0] > $p[2])	$alert = "Upper constraint of ".$p[2]." violated";
							if ($v[0] < $p[3])	$alert = "Lower constraint of ".$p[2]." violated";

							$gauges[] = array(
												"name" => $p[4],
												"value" => (($v[1] == "1") ? floatval(round($v[0], 2)) : 0),
												"upper" => (($p[2] != "") ? floatval(round($p[2], 2)) : 0),
												"lower" => (($p[3] != "") ? floatval(round($p[3], 2)) : 0),
												"id" => $p[0],
												"alert" => $alert
											);
						}
					}
					else	{
						$gauges[] = array(
											"name" => $p[4],
											"value" => 0,
											"upper" => (($p[2] != "") ? floatval(round($p[2], 2)) : 0),
											"lower" => (($p[3] != "") ? floatval(round($p[3], 2)) : 0),
											"id" => $p[0],
											"alert" => $alert
										);
					}
				} else	{
					$gauges[] = array(
										"name" => $p[4],
										"value" => 0,
										"upper" =>1,
										"lower" => 0,
										"id" => $p[0],
										"alert" => $alert
									);
				}
            }
		}

		return $gauges;
	}

	function getNodeIndicators($node)	{
		$usertypeID = $this->getVariable("usertypeID");
		$projectID = $this->getVariable("projectID");
		$nodeID = $node[0];

		$nodeIndicators = array();

		if ($usertypeID == "0")
			//$parameters = array("CRITICAL_INDICATOR", "DIGITAL_IN", "DIGITAL_OUT");
			$parameters = array("DIGITAL_IN", "DIGITAL_OUT");
		else if ($usertypeID == $this->dbo->q("SELECT id FROM usertypes WHERE typeDescr = 'Power User'"))
			//$parameters = array("CRITICAL_INDICATOR");
			$parameters = "";
		else
			$parameters = "";

		if (is_array($parameters))	{
			foreach ($parameters as $parameter)	{
				$column = ($parameter == "DIGITAL_IN") ? "digitalIn" : (($parameter == "DIGITAL_OUT") ? "digitalOut" : "digitalCrit");

				$title = ($parameter == "DIGITAL_IN") ? "- Digital Input Status Indicators -"
														: (($parameter == "DIGITAL_OUT") ? "- Digital Output Status Indicators -"
																							: "- Critical Parameter Status Indicators -");
				$descr = ($parameter == "DIGITAL_IN") ? "Input from pump system to PLC."
														: (($parameter == "DIGITAL_OUT") ? "Output from PLC to pump system."
																							: "Indicates which parameter was responsible for tripping the pump.");

				$sql = "SELECT bit, $column FROM NodeIndicator WHERE projectID = '".$projectID."' AND nodeID = '".$nodeID."' AND $column != '' ORDER BY bit ASC";

				$indicators = $this->dbo->q($sql);

				$nodeIndicators[$column]["title"] = $title;
				$nodeIndicators[$column]["descr"] = $descr;

				if (is_array($indicators))	{
					foreach ($indicators as $indicator)	{
						$class = ($this->checkRXTX($nodeID) != Functions_boynton::RED) ? $this->checkBit($nodeID, $parameter, ($indicator[0] - 1)) : Functions_boynton::GREY;

						$nodeIndicators[$column]["data"][] = array("class" => $class, "descr" => $indicator[1]);
					}
				}
				else	{
					$nodeIndicators[$column]["data"][] = array("class" => Functions_boynton::GREY, "descr" => "No Indicators Defined In Indicator Section");
				}
			}
		}

		return $nodeIndicators;
	}

	function getNodeControls($node)	{
		$projectID = $this->getVariable("projectID");
		$usertypeID = $this->getVariable("usertypeID");
		$nodeID = $node[0];

		$controls = array();

		if ($usertypeID == 0 || $usertypeID == $this->dbo->q("SELECT id FROM usertypes WHERE typeDescr = 'Power User'"))	{
			//  TURN PUMP ON/OFF
			$status = $this->dbo->q("SELECT status, locked FROM BoreholeRunningStatus WHERE nodeID = '".$nodeID."' AND projectID = '".$projectID."'");
			$status = $status[0];

			$controls[] = array(
								"heading" => "Pump Auto Control",
								"type" => "radio",
								"class" => "power",
								"id" => $nodeID,
								"options" => array(
												array(
													"value" => "On",
													"descr" => "Turn Pump On",
													"checked" => (($status[0] == "ON") ? "checked" : ""),
													"disabled" => (($status[1] == "1") ? "disabled" : "")
													),
												array(
													"value" => "Off",
													"descr" => "Turn Pump Off",
													"checked" => (($status[0] == "OFF") ? "checked" : ""),
													"disabled" => (($status[1] == "1") ? "disabled" : "")
													)
												)
							);

			$tripped = $this->dbo->q("SELECT locked FROM TripButtonStatus WHERE nodeID = '".$nodeID."' AND projectID = '".$projectID."'");
			$controls[] = array(
								"heading" => "Untrip Borehole",
								"type" => "button",
								"class" => "untrip",
								"id" => $nodeID,
								"options" => array(
												array(
													"value" => "Untrip",
													)
												)
							);
		}

		return $controls;
	}

	function getNodeDetails($node)	{
		$projectID = $this->getVariable("projectID");
		$tstamp = date("Y-m-d");

		$headingTypes["PLC"] = array("rxtx","status","tripped","constraints");
		$headings = $headingTypes[$node[2]];

		$index = 0;
		$tmpArr = array();
		$tmpArr[$index++] = count($headings) + 3;
		$tmpArr[$index++] = $node[0];
		$tmpArr[$index++] = $node[1];

		if (is_array($headings))	{
			foreach ($headings as $h)	{
				switch ($h)	{
					case "rxtx":	{
						$tmpArr[$index++] = array("class" => $this->checkRXTX($node[0]), "val" => "");
						break;
					}
					case "status":	{
						if (!($node[1] == "FF6_Booster1" || $node[1] == "FF6_Booster2"))
							$value = ($this->checkRXTX($node[0]) != Functions_boynton::RED) ? $this->checkStatus($node[0]) : Functions_boynton::GREY;
						else
							$value = ($this->checkRXTX($node[0]) != Functions_boynton::RED) ? (($this->checkStatus($node[0]) != Functions_boynton::RED) ? $this->checkStatus($node[0]) : Functions_boynton::GREY) : Functions_boynton::GREY;

						if (!($node[1] == "FF6_Main"))
							$tmpArr[$index++] = array("class" => $value, "val" => "");
						else
							$tmpArr[$index++] = "";
						break;
					}
					case "tripped":	{
						if (!($node[1] == "FF6_Booster1" || $node[1] == "FF6_Booster2" || $node[1] == "FF6_Main"))
							$tmpArr[$index++] = ($this->checkRXTX($node[0]) != Functions_boynton::RED) ? array("class" => $this->checkTripped($node[0]), "val" => "") : array("class" => Functions_boynton::GREY, "val" => "");
						else
							$tmpArr[$index++] = "";
						break;
					}
					case "constraints":	{
						$value = ($this->checkRXTX($node[0]) != Functions_boynton::RED) ? $this->checkConstraints($node[0]) : Functions_boynton::GREY;

						if (!($node[1] == "FF6_Main"))
							$tmpArr[$index++] = array("class" => $value, "val" => "");
						else
							$tmpArr[$index++] = "";
						break;
					}
					default:
						break;
				}
			}
		}

		if (!($node[1] == "FF6_Main" || $node[1] == "Main_RES" || $node[1] == "FF6_Booster1" || $node[1] == "FF6_Booster2" ))	{
			$sql = "SELECT dailyFlow FROM DailyCumulativeFlow WHERE projectID = '".$projectID."' AND node = '".$node[0]."' AND tstamp = '".$tstamp."'";

			$flow = $this->dbo->q($sql);

			$tmpArr[$index++] = number_format((double)$flow,2,".","");
		}
		else
			$tmpArr[$index++] = "-";

		return $tmpArr;
	}
}