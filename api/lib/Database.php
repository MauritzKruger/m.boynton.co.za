<?php
class Database	{
    var $dbn;

    function __construct()	{
    }

    function getConnection() {
        $dbhost = "localhost";
        $dbuser = "scada";
        $dbpass = "m0n170r1n6";
        $dbname = "scada";

        $this->dbn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        $this->dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $this->dbn;
    }

    //  SQL Query Run Procedure
    function q($sql)	{
		try {
			$db = $this->getConnection();
			$stmt = $db->prepare($sql);
			$stmt->execute();

			if (strtolower(substr($sql, 0, 6)) != "select")	{
				return $stmt->rowCount();
			} else	{
				$resultSet = $stmt->fetchAll(PDO::FETCH_NUM);
				$db = null;

				$row_count = $stmt->rowCount();
				$column_count = $stmt->columnCount();

				if (!$row_count)	return 0;

				$result = array();

				if ($row_count == 1 && $column_count == 1)	return $resultSet[0][0];

				if ($row_count == 1)	{
					for ($j = 0; $j < $column_count; $j++)	{
						$result[0][$j] = $resultSet[0][$j];
					}
				} else	{
					for ($i = 0; $i < $row_count; $i++)	{
						for ($j = 0; $j < $column_count; $j++)	{
							$result[$i][$j] = $resultSet[$i][$j];
						}
					}
				}

				return $result;
			}
		} catch(PDOException $e) {
			echo "{'error':{'text':". $e->getMessage() ."}}";
		}
	}

	function getFuncPrefix()	{
		if (isset($_SESSION["projectID"]))	{
			if ($_SESSION["projectID"] == $this->q("SELECT id FROM projects WHERE name = 'Pilanesberg Platinum Mines Water Monitoring'"))
				return "func_boynton";
			else
				return "func_sewerage";
		}
		else
			return "func_boynton";
	}

	function sort_array($array, $column)	{
        $size = count($array);

        for ($a = 0; $a < ($size - 1); $a++) {
            for ($b = $a + 1; $b < $size; $b++) {
                if ($array[$a][$column] == $array[$b][$column]) {
                    if ($array[$a][$column] < $array[$b][$column]) {
                        $keep = $array[$a];
                        $array[$a] = $array[$b];
                        $array[$b] = $keep;
                    }
                }
                else if ($array[$a][$column] > $array[$b][$column]) {
                    $keep = $array[$a];
                    $array[$a] = $array[$b];
                    $array[$b] = $keep;
                }
            }
        }

        return $array;
	}
}