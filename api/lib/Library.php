<?php
	session_start();

	require "lib/Database.php";
	require "lib/Functions_boynton.php";
	require "lib/Functions_sewerage.php";
	require "classes/Offices.php";
	require "classes/Projects.php";
	require "classes/Login.php";
	require "classes/Menus.php";
	require "classes/Node.php";
	require "classes/Graphs.php";
