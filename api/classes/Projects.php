<?php
class Projects	{
    var $dbo;
    var $func_boynton;
    var $func_sewerage;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

	public function getProjects($officeID)	{
		unset($result);

		$func = $this->dbo->getFuncPrefix();
		$usertypeID = $this->$func->getVariable("usertypeID");
		$userID = $this->$func->getVariable("userID");
		$projectID = $this->$func->getVariable("projectID");

		if ($usertypeID == "0")
			$projects = $this->dbo->q("SELECT p.id, p.name ".
										"FROM (projects AS p INNER JOIN officeProject AS op ON p.id = op.projectID) ".
										"WHERE op.officeID = '".$officeID."' ".
										"ORDER BY p.name");
		else
			$projects = $this->dbo->q("SELECT p.id, p.name ".
										"FROM ((projects AS p INNER JOIN officeProject AS op ON p.id = op.projectID) ".
											"INNER JOIN projectUser AS pu ON p.id = pu.projectID) ".
										"WHERE op.officeID = '".$officeID."' AND pu.userID = '".$userID."' ".
										"ORDER BY p.name");

		if (is_array($projects))	{
			foreach ($projects as $project)	{
				$result["projects"][] = array(
															"id" => $project[0],
															"name" => $project[1],
															"selected" => ($project[0] == $projectID) ? true : false
														);
			}
		}

		echo json_encode($result);
	}


	public function setProject($officeID = "", $projectID = "")	{
		$func = $this->dbo->getFuncPrefix();
		$this->$func->setVariable("officeID",$officeID);
		$this->$func->setVariable("projectID",$projectID);

		echo json_encode("");
	}
}