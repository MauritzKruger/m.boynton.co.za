<?php
class Login	{
	const SALT_LENGTH = 9;

	var $dbo;
    var $func_boynton;
    var $func_sewerage;

	var $attempt;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

	function generateHash($plainText, $salt = null)	{
		if ($salt === null)	$salt = substr(md5(uniqid(rand(), true)), 0, Login::SALT_LENGTH);
		else				$salt = substr($salt, 0, Login::SALT_LENGTH);

		return $salt.hash("sha512", ($salt.$plainText));
	}

	function checkLogin()	{
		$func = $this->dbo->getFuncPrefix();
		$user = $this->$func->getVariable("user");
		$userID = $this->$func->getVariable("userID");

		if ($user == "admin")	$this->$func->setVariable("usertypeID","0");

		if ($user == "admin")	$offices = $this->dbo->q("SELECT id, name FROM offices ORDER BY name");
		else				$offices = $this->dbo->q("SELECT o.id, o.name FROM (offices AS o INNER JOIN officeUser AS ou ON o.id = ou.officeID) ".
											"WHERE ou.userID = '".$userID."' ORDER BY o.name");

		if (is_array($offices) && count($offices) > 1)	{
			$this->$func->setVariable("officeCount",count($offices));

			return;
		} else if (is_array($offices) && count($offices) == 1)	{
			$this->$func->setVariable("officeCount",count($offices));
			$this->$func->setVariable("officeID",$offices[0][0]);

			if ($user != "admin")	$this->$func->setVariable("usertypeID",$this->dbo->q("SELECT usertypeID FROM userAssosiation WHERE officeID = '".$offices[0][0]."' AND userID = '".$userID."'"));

			if ($user == "admin")	$projects = $this->dbo->q("SELECT p.id, p.name FROM (projects AS p INNER JOIN officeProject AS op ON p.id = op.projectID) ".
																						"WHERE op.officeID = '".$offices[0][0]."'");
            else							$projects = $this->dbo->q("SELECT p.id, p.name ".
																						"FROM ((projects AS p INNER JOIN officeProject AS op ON p.id = op.projectID) INNER JOIN projectUser AS pu ON p.id = pu.projectID) ".
																						"WHERE op.officeID = '".$offices[0][0]."' AND pu.userID = '".$userID."'");

            if (is_array($projects) && count($projects) > 1)	{
				$this->$func->setVariable("projectCount",count($projects));

				return;
            } else	{
				$this->$func->setVariable("projectCount",count($projects));
				$this->$func->setVariable("projectID",$projects[0][0]);

				return;
            }
        }

        return;
	}

	public function isLoggedIn()	{
		$func = $this->dbo->getFuncPrefix();

		echo json_encode($this->$func->getVariable("loggedIn"));
	}

	public function doLogin($email = "", $password = "")	{
		$func = $this->dbo->getFuncPrefix();

		if (isset($this->attempt))	sleep($this->attempt);

		$username = strip_tags($email);
		$salt = $this->dbo->q("SELECT salt FROM users WHERE email = '".$username."'");
		$password = $this->generateHash($password, $salt);
		$login = $this->dbo->q("SELECT id, email FROM users WHERE email = '".$username."' AND password = '".$password."'");

		//  Username & Password Match
		if (is_array($login))	{
			unset($this->attempt);

			$this->$func->setVariable("loggedIn",true);
			$this->$func->setVariable("userID",$login[0][0]);
			$this->$func->setVariable("user",$login[0][1]);

			$this->checkLogin();

			$redirect = "projectsMode";
			$redirect_func = "getData";

			if ($this->$func->getVariable("officeCount") == 1 && $this->$func->getVariable("projectCount") == 1)	{
				$redirect = "statusMode";
			}

			echo json_encode(array("loggedIn" => $this->$func->getVariable("loggedIn"), "redirectTo" => $redirect, "func" => $redirect_func));
		} else	{
				$this->$func->setVariable("loggedIn",false);

                if (!isset($this->attempt)) $this->attempt= 1;
                else    $this->attempt = $this->attempt * 2;

				echo json_encode(array("loggedIn" => $this->$func->getVariable("loggedIn"), "type" => "FAILED", "message" => "Sorry, wrong username/password"));
		}
	}

	function doLogout()	{
		session_destroy();

		echo json_encode("");
	}
}