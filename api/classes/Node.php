<?php
class Node	{
	var $dbo;
    var $func_boynton;
    var $func_sewerage;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

    function time_since($since) {
		$since = time() - strtotime($since);

		$chunks = array(
									array(60 * 60 * 24 * 365 , 'year'),
									array(60 * 60 * 24 * 30 , 'month'),
									array(60 * 60 * 24 * 7, 'week'),
									array(60 * 60 * 24 , 'day'),
									array(60 * 60 , 'hour'),
									array(60 , 'minute'),
									array(1 , 'second')
								);

		for ($i = 0, $j = count($chunks); $i < $j; $i++)	{
			$seconds = $chunks[$i][0];
			$name = $chunks[$i][1];

			if (($count = floor($since / $seconds)) != 0)	break;
		}

		return ($count == 1) ? "$count {$name} ago" : "$count {$name}s ago";
    }

	function getTwitterPosts()	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$data = array();

		$tweets = $this->dbo->q("SELECT date, post FROM twitterposts WHERE projectID = '".$projectID."' ORDER BY date DESC LIMIT 3");

		if (is_array($tweets))	{
			foreach ($tweets as $tweet)	{
				$data[] = "<b>".$this->time_since($tweet[0])."</b> - ".$tweet[1];
			}
		}

		return $data;
	}

	function getNodes() {
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, nt.name FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
				"WHERE n.projectID = '".$projectID."' AND n.displayName != 'Main_RES' ORDER BY nt.id ASC, n.ranking ASC, n.displayName ASC";

		$nodes = $this->dbo->q($sql);

		$data = array();

		if (is_array($nodes) && count($nodes) > 0)	{
			$data["nodeInfoHeadings"] = $this->$func->getInfoHeadings();

			foreach ($nodes as $node)	{
				$data["nodeInfo"][] = $this->$func->getNodeDetails($node);
			}
		}

		if ($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Hartbeespoort Sewerage'"))
			$data["timers"] = $this->$func->getTimers();

		if ($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Pilanesberg Platinum Mines Water Monitoring'"))
			$data["totalVolume"] = array("Total Daily Volume ".$this->$func->getTotalDailyVolume()."m<sup>3</sup>");

		$data["twitterPosts"] = $this->getTwitterPosts();

		echo json_encode($data);
	}

	function getReportNodes()	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, false FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
				"WHERE n.projectID = '".$projectID."' AND n.displayName NOT IN ('Main_RES') ORDER BY nt.name DESC, n.displayName ASC";

		$nodes = $this->dbo->q($sql);

		echo json_encode($nodes);
	}

	function getFlowReportNodes()	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, false FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
				"WHERE n.projectID = '".$projectID."' AND n.displayName IN ('Rietfontein Inlet', 'Rietfontein Outlet') ORDER BY nt.name DESC, n.displayName ASC";

		$nodes = $this->dbo->q($sql);

		echo json_encode($nodes);
	}

	function getReportParameters($nodes = "")	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		if ($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Pilanesberg Platinum Mines Water Monitoring'"))	{
			$data["parameters"] = array(
														array("value" => "CURRENT", "descr" => "Current [A]"),
														array("value" => "FLOW", "descr" => "Flow [m3/h]"),
														array("value" => "PRESSBEFORE", "descr" => "Pressure Before [Bar]"),
														array("value" => "PRESSAFTER", "descr" => "Pressure After [Bar]"),
														array("value" => "SIGNAL_STRENGTH", "descr" => "Signal Strength"),
														array("value" => "VOLT", "descr" => "Volt [V]"),
														array("value" => "WATERLEVEL", "descr" => "Water Level [m]")
													);
		} else	{
			$nodes = preg_split("/__/", $nodes);

			$used = array();

			if (is_array($nodes))	{
				foreach ($nodes as $node)	{
					$parameters = $this->dbo->q("SELECT name,displayName FROM Parameter ".
																	"WHERE projectID = '".$projectID."' AND nodeID = '".$node."' ".
																		"AND (active = '1' OR name = 'Priority' OR name = 'Oscillation_Timer' OR name = 'Over_Capacity_Timer' OR name = 'Recovery_Timer') ".
																	"ORDER BY displayName ASC");

					if (is_array($parameters))	{
						foreach ($parameters as $parameter)	{
							if (!in_array($parameter[0], $used))	{
								$used[] = $parameter[0];
								$data["parameters"][] = array("value" => $parameter[0], "descr" => $parameter[1]);
							}
						}
					}
				}
			}
		}

		$data["parameters"] = $this->dbo->sort_array($data["parameters"], "descr");

		echo json_encode($data);
	}

	function getAutoControlNodes()	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, brs.status, brs.locked ".
				"FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id ".
					"INNER JOIN BoreholeRunningStatus AS brs ON n.id = brs.nodeID) ".
				"WHERE n.projectID = '".$projectID."' AND n.displayName NOT IN ('FF6_Booster1','FF6_Booster2','FF6_Main','Main_RES') ORDER BY nt.id ASC, n.ranking ASC, n.displayName ASC";

		$nodes = $this->dbo->q($sql);

		echo json_encode($nodes);
	}

	function getNode($id) {
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$sql = "SELECT n.id, n.displayName, nt.name FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
				"WHERE n.projectID = '".$projectID."' AND n.id = '".$id."'";

		$node = $this->dbo->q($sql);

		$data = array();

		$data["nodeName"] = $node[0][1];
		$data["nodeControl"] = $this->$func->getNodeControls($node[0]);
		$data["nodeInfoHeadings"] = $this->$func->getInfoHeadings();
		$data["nodeInfo"] = array($this->$func->getNodeDetails($node[0]));
		$data["nodeGauges"] = $this->$func->getNodeGauges($node[0]);
		$data["nodeIndicators"] = $this->$func->getNodeIndicators($node[0]);

		echo json_encode($data);
	}

	function runFunction($function = "", $id = "", $control = "")	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");
		$userID = $this->$func->getVariable("userID");
		$user = $this->$func->getVariable("user");
		$result = array("type" => "", "message" => "");

		switch ($function)	{
			case "powerOnOff":
				if ($id !== "all")	{
					if ($this->dbo->q("UPDATE BoreholeRunningStatus SET status = '".strtoupper($control)."' WHERE nodeID = '".$id."' AND projectID = '".$projectID."'") > 0)
						$result = array("type" => "SUCCESS", "message" => strtoupper($control)." command issued successfully.");
					else
						$result = array("type" => "FAILED", "message" => strtoupper($control)." command issue failed.");
				} else	{
					$count = 0;

					$sql = "SELECT n.id, n.displayName, nt.name FROM (Node AS n INNER JOIN NodeType AS nt ON n.nodeTypeID = nt.id) ".
							"WHERE n.projectID = '".$projectID."' AND n.displayName != 'Main_RES' ORDER BY nt.id ASC, n.ranking ASC, n.displayName ASC";

					$nodes = $this->dbo->q($sql);

					if (is_array($nodes) && count($nodes) > 0)	{
						foreach ($nodes as $node)	{
							if ($this->dbo->q("UPDATE BoreholeRunningStatus SET status = '".strtoupper($control)."' WHERE nodeID = '".$node[0]."' AND projectID = '".$projectID."'") > 0)
								$count++;
						}
					}

					if ($count > 0)
						$result = array("type" => "SUCCESS", "message" => strtoupper($control)." command issued successfully.");
					else
						$result = array("type" => "FAILED", "message" => strtoupper($control)." command issue failed.");
				}
				break;
			case "lockOnOff":
				$command = ($control == 1) ? "Lock" : "Unlock";

				if ($this->dbo->q("UPDATE BoreholeRunningStatus SET locked = '".$control."' WHERE nodeID = '".$id."' AND projectID = '".$projectID."'") > 0)
					$result = array("type" => "SUCCESS", "message" => $command." command issued successfully.");
				else
					$result = array("type" => "FAILED", "message" => $command." command issue failed.");
				break;
			case "unTrip":
				if ($this->dbo->q("INSERT INTO control (projectID,nodeID,controlID,onoff,payload,processed,tstamp,byUser) VALUES ".
							"(".$projectID.",".$id.",(SELECT id FROM plcControl WHERE name = 'TRIPPED_CB'),0,'',0,'".date("Y-m-d H:i:s")."','".$user."')") > 0)	{
					$result = array("type" => "SUCCESS", "message" => "Untrip command issued successfully.");

					$node = $this->dbo->q("SELECT displayName FROM Node WHERE projectID = '".$projectID."' AND id = '".$id."'");

					$this->dbo->q("INSERT INTO logss (projectID, userID, descr, access, ontable, date) VALUES ".
									"('".$projectID."', '".$userID."', 'Untrip command issued for ".$node."', 'INSERT', 'control', '".date("Y-m-d H:i:s")."')");
				}
				else
					$result = array("type" => "FAILED", "message" => "Untrip command issue failed.");
				break;
			default:
				break;
		}

		if ($result["type"] == "SUCCESS")	{
			$this->dbo->q("UPDATE dbstatus SET status = 'updated'");
			$this->dbo->q("UPDATE dbStatusNotify SET status = 'updated'");
		}

		echo json_encode($result);
	}
}