<?php
class Offices	{
    var $dbo;
    var $func_boynton;
    var $func_sewerage;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

	public function getOffices()	{
		unset($result);

		$func = $this->dbo->getFuncPrefix();
		$usertypeID = $this->$func->getVariable("usertypeID");
		$userID = $this->$func->getVariable("userID");

		if ($usertypeID == "0")
			$offices = $this->dbo->q("SELECT id, name FROM offices ORDER BY name");
		else
			$offices = $this->dbo->q("SELECT o.id, o.name FROM (offices AS o INNER JOIN officeUser AS ou ON o.id = ou.officeID) WHERE ou.userID = '".$userID."' ORDER BY o.name");

		if (is_array($offices))	{
			foreach ($offices as $office)	{
				$result["offices"][] = array(
											"id" => $office[0],
											"name" => $office[1]
										);
			}
		}

		echo json_encode($result);
	}
}