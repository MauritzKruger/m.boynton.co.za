<?php
class Menus	{
	var $dbo;
    var $func_boynton;
    var $func_sewerage;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

	public function getMenus()	{
		$display = false;

		$func = $this->dbo->getFuncPrefix();
		$usertypeID = $this->$func->getVariable("usertypeID");
		$projectID = $this->$func->getVariable("projectID");

		if ($this->$func->getVariable("officeCount") > 1)	{
			$display = true;
		} else if ($this->$func->getVariable("officeCount") == 1)	{
			if ($this->$func->getVariable("projectCount") > 1)
				$display = true;
		}

		if ($display === true) {
			if (is_numeric($this->$func->getVariable("projectID")))	{
				$menus = ($usertypeID == 0 || $usertypeID == $this->dbo->q("SELECT id FROM usertypes WHERE typeDescr = 'Power User'")) ?
								(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Pilanesberg Platinum Mines Water Monitoring'")) ?
									array(
										array("descr" => "Projects", "id" => "projects"),
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Reports", "id" => "reports"),
										array("descr" => "Control", "id" => "auto_control"),
										array("descr" => "Logout", "id" => "signout")
									) :
									(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Hartbeespoort Sewerage'")) ?
									array(
										array("descr" => "Projects", "id" => "projects"),
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Report Graphs", "id" => "reports"),
										array("descr" => "Report Flow", "id" => "report_flow"),
										array("descr" => "Logout", "id" => "signout")
									) :
									array(
										array("descr" => "Projects", "id" => "projects"),
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Reports", "id" => "reports"),
										array("descr" => "Logout", "id" => "signout")
									))
								) :
								(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Hartbeespoort Sewerage'")) ?
									array(
										array("descr" => "Projects", "id" => "projects"),
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Report Graphs", "id" => "reports"),
										array("descr" => "Report Flow", "id" => "report_flow"),
										array("descr" => "Logout", "id" => "signout")
									) :
									array(
										array("descr" => "Projects", "id" => "projects"),
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Reports", "id" => "reports"),
										array("descr" => "Logout", "id" => "signout")
									)
								);
			}	else	{
				$menus = array(
									array("descr" => "Projects", "id" => "projects"),
									array("descr" => "Logout", "id" => "signout")
								);
			}
		} else	{
			$menus = ($usertypeID == 0 || $usertypeID == $this->dbo->q("SELECT id FROM usertypes WHERE typeDescr = 'Power User'")) ?
							(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Pilanesberg Platinum Mines Water Monitoring'")) ?
								array(
									array("descr" => "Home", "id" => "status"),
									array("descr" => "Reports", "id" => "reports"),
									array("descr" => "Control", "id" => "auto_control"),
									array("descr" => "Logout", "id" => "signout")
								) :
								(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Hartbeespoort Sewerage'")) ?
									array(
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Report Graphs", "id" => "reports"),
										array("descr" => "Report Flow", "id" => "report_flow"),
										array("descr" => "Logout", "id" => "signout")
									) :
									array(
										array("descr" => "Home", "id" => "status"),
										array("descr" => "Reports", "id" => "reports"),
										array("descr" => "Logout", "id" => "signout")
									))
							) :
							(($projectID == $this->dbo->q("SELECT id FROM projects WHERE name = 'Hartbeespoort Sewerage'")) ?
								array(
									array("descr" => "Home", "id" => "status"),
									array("descr" => "Report Graphs", "id" => "reports"),
									array("descr" => "Report Flow", "id" => "report_flow"),
									array("descr" => "Logout", "id" => "signout")
								) :
								array(
									array("descr" => "Home", "id" => "status"),
									array("descr" => "Reports", "id" => "reports"),
									array("descr" => "Logout", "id" => "signout")
								)
							);
		}

		echo json_encode($menus);
	}
}