<?php
class Graphs	{
    var $dbo;
    var $func_boynton;
    var $func_sewerage;

    function __construct()	{
        $this->dbo = new Database();
        $this->func_boynton = new Functions_boynton();
        $this->func_sewerage = new Functions_sewerage();
    }

	function getGraphData($nodes= "", $params = "", $period = "")	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$dateFrom = "";
		$dateTo = "";

		$nodes = preg_split("/__/", $nodes);
		$params = preg_split("/__/", $params);

		if ($period == "last3")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-3 hours"));
		} else if ($period == "last6")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-6 hours"));
		} else if ($period == "last12")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-12 hours"));
		} else if ($period == "last24")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-1 day"));
		} else if ($period == "currWeek")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-7 days"));
		} else if ($period == "prevWeek")	{
			$dateTo = date("Y-m-d H:i:s", strtotime("-1 week"));
			$dateFrom = date("Y-m-d H:i:s", strtotime("-2 weeks"));
		} else if ($period == "currMonth")	{
			$dateTo = date("Y-m-d H:i:s");
			$dateFrom = date("Y-m-d H:i:s", strtotime("-1 month"));
		} else if ($period == "prevMonth")	{
			$dateTo = date("Y-m-d H:i:s", strtotime("-1 month"));
			$dateFrom = date("Y-m-d H:i:s", strtotime("-2 months"));
		}

		$results = array();

		if (is_array($params))	{
			foreach ($params as $param)	{
				if (is_array($nodes))	{
					foreach ($nodes as $node)	{
						$data = array();

						$descr = $this->dbo->q("SELECT displayName FROM Parameter WHERE name = '".$param."' AND nodeID = '".$node."'");
						$label = $this->dbo->q("SELECT displayName FROM Node WHERE id = '".$node."'");

						if ($descr != "" && $descr != "0")	$results[$param]["descr"] = $descr;
						if ($label != "" && $label != "0")		$results[$param][$node]["label"] = $label;

						$result = $this->dbo->q("SELECT tstamp, value ".
																"FROM sampleHistory ".
																"WHERE projectID = '".$projectID."' ".
																	"AND nodeID = '".$node."' ".
																	"AND parameterID = (SELECT id FROM Parameter WHERE name = '".$param."' AND nodeID = '".$node."') ".
																	"AND tstamp BETWEEN '".$dateFrom."' AND '".$dateTo."' ".
																"ORDER BY tstamp");

						if (is_array($result))	{
							foreach ($result as $r)	{
								$data[] = array(
															"year" => date("Y", strtotime($r[0])) * 1,
															"month" => (date("m", strtotime($r[0])) * 1) - 1,
															"day" => date("d", strtotime($r[0])) * 1,
															"hours" => date("H", strtotime($r[0])) * 1,
															"minutes" => date("i", strtotime($r[0])) * 1,
															"seconds" => date("s", strtotime($r[0])) * 1,
															"value" => $r[1]
														);
							}

							$results[$param][$node]["data"] = $data;
						}
					}
				}
			}
		}

		echo json_encode($results);
	}

	function getFlowData($nodes= "", $period = "")	{
		$func = $this->dbo->getFuncPrefix();
		$projectID = $this->$func->getVariable("projectID");

		$dateFrom = "";
		$dateTo = "";

		$nodes = preg_split("/__/", $nodes);

		if ($period == "yesterday")	{
			$dateTo = date("Y-m-d", strtotime("-1 day"));
			$dateFrom = date("Y-m-d", strtotime("-1 day"));
		} else if ($period == "currWeek")	{
			$dateTo = date("Y-m-d");
			$dateFrom = date("Y-m-d", strtotime("-7 days"));
		} else if ($period == "prevWeek")	{
			$dateTo = date("Y-m-d", strtotime("-1 week"));
			$dateFrom = date("Y-m-d", strtotime("-2 weeks"));
		} else if ($period == "currMonth")	{
			$dateTo = date("Y-m-d");
			$dateFrom = date("Y-m-d", strtotime("-1 month"));
		} else if ($period == "prevMonth")	{
			$dateTo = date("Y-m-d", strtotime("-1 month"));
			$dateFrom = date("Y-m-d", strtotime("-2 months"));
		}

		$results = array();

		if (is_array($nodes))	{
			foreach ($nodes as $node)	{
				$data = array();

				$label = $this->dbo->q("SELECT displayName FROM Node WHERE id = '".$node."'");

				if ($label != "" && $label != "0")		$results[$node]["label"] = $label;

				$result = $this->dbo->q("SELECT tstamp, dailyFlow ".
														"FROM DailyCumulativeFlow ".
														"WHERE projectID = '".$projectID."' ".
															"AND node = '".$node."' ".
															"AND tstamp BETWEEN '".$dateFrom."' AND '".$dateTo."' ".
														"ORDER BY tstamp");

				if (is_array($result))	{
					foreach ($result as $r)	{
						$data[] = array(
													"year" => date("Y", strtotime($r[0])) * 1,
													"month" => (date("m", strtotime($r[0])) * 1) - 1,
													"day" => date("d", strtotime($r[0])) * 1,
													"hours" => date("H", strtotime($r[0])) * 1,
													"minutes" => date("i", strtotime($r[0])) * 1,
													"seconds" => date("s", strtotime($r[0])) * 1,
													"value" => $r[1]
												);
					}

					$results[$node]["data"] = $data;
				}
			}
		}

		echo json_encode($results);
	}
}