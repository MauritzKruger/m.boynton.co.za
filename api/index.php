<?php
date_default_timezone_set("Africa/Johannesburg");

require_once "vendor/autoload.php";
require_once "functions/main.php";
require "lib/Library.php";

\Slim\Slim::registerAutoloader();
Twig_Autoloader::register();

$app = new \Slim\Slim();

// GET routes
$app->get("/login", "isLoggedIn");

$app->get("/offices", "getOffices");

$app->get("/projects/:officeID", "getProjects");

$app->get("/menus", "getMenus");

$app->get("/nodes", "getNodes");
$app->get("/report_nodes", "getReportNodes");
$app->get("/report_flow_nodes", "getFlowReportNodes");
$app->get("/report_parameters/:nodes", "getReportParameters");
$app->get("/auto_control_nodes", "getAutoControlNodes");

$app->get("/nodes/:id", "getNode");

// POST routes
$app->post("/login/:email/:password", "doLogin");

$app->post("/runFunction/:func/:id/:control", "runFunction");

$app->post("/set/:officeID/:projectID", "setProject");

$app->post("/report_data/:nodes/:params/:period", "getGraphData");
$app->post("/report_flow_data/:nodes/:period", "getFlowData");

// PUT routes

// DELETE routes
$app->delete("/logout", "doLogout");

$app->run();