<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nomad
 * Date: 2012/12/16
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */

//  GET Function(s)
function isLoggedIn() {
    $l = new Login();
    $result = $l->isLoggedIn();
    echo $result;
}

function getMenus() {
    $m = new Menus();
    $result = $m->getMenus();
    echo $result;
}

function getOffices() {
    $o = new Offices();
    $result = $o->getOffices();
    echo $result;
}

function getProjects($officeID) {
    $p = new Projects();
    $result = $p->getProjects($officeID);
    echo $result;
}

function getNodes() {
    $n = new Node();
    $result = $n->getNodes();
    echo $result;
}

function getReportNodes() {
    $n = new Node();
    $result = $n->getReportNodes();
    echo $result;
}

function getFlowReportNodes() {
    $n = new Node();
    $result = $n->getFlowReportNodes();
    echo $result;
}

function getReportParameters($nodes) {
    $n = new Node();
    $result = $n->getReportParameters($nodes);
    echo $result;
}

function getAutoControlNodes() {
    $n = new Node();
    $result = $n->getAutoControlNodes();
    echo $result;
}

function getNode($id) {
    $n = new Node();
    $result = $n->getNode($id);
    echo $result;
}

//  POST Function(s)
function doLogin($email, $password) {
    $l = new Login();
    $result = $l->doLogin($email, $password);
    echo $result;
}

function runFunction($func, $id, $control) {
    $n = new Node();
    $result = $n->runFunction($func, $id, $control);
    echo $result;
}

function setProject($officeID, $projectID) {
    $p = new Projects();
    $result = $p->setProject($officeID, $projectID);
    echo $result;
}

function getGraphData($nodes, $params, $period) {
    $g = new Graphs();
    $result = $g->getGraphData($nodes, $params, $period);
    echo $result;
}

function getFlowData($nodes, $period) {
    $g = new Graphs();
    $result = $g->getFlowData($nodes, $period);
    echo $result;
}

//  DELETE Function(s)
function doLogout()	{
    $l = new Login();
    $result = $l->doLogout();
    echo $result;
}